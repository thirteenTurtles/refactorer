open Containers

open Compiler
open   Compmisc
open   Config
open   Location
open   Typedtree

open Frontend

open Rotor
open Sourcefile
open Ast_visitors

module Htbl = Hashtbl.Make'(String)

let run codebase =
  let ident_map : int Htbl.t = Htbl.create 1000 in
  let iter =
    object (self)
      inherit [_] Typedtree_visitors.iter as super
      method! visit_tt_expression env e =
        match e.exp_desc with
        | Texp_ident (p, _, _)
            when not e.exp_loc.loc_ghost ->
          let p =
            try
              Env.normalize_module_path (Some e.exp_loc) e.exp_env p
            with ((Env.Error e) as err) ->
              let () =
                prerr_endline
                  (Format.sprintf
                    "Error normalizing path %a: %a"
                      Printtyp.path p
                      Env.report_error e) in
              raise err
            in
          if Ident.persistent (Path.head p) then
          begin try
            let _ = Env.find_value p e.exp_env in
            let p = Printtyp.string_of_path p in
            let n = Htbl.get_or ~default:0 ident_map p in
            Htbl.replace ident_map p (n + 1)
          with Not_found -> () end
        | _ ->
          super#visit_tt_expression env e
    end in
  let () =
    codebase |> Codebase.iter @@ fun f ->
    let () = prerr_endline (Format.sprintf "Processing %s" f.Fileinfos.filename) in
    match (of_fileinfos f).ast with
    | Interface (_, Some _sig) ->
      iter#visit_tt_signature () _sig
    | Implementation (_, Some _struct) ->
      iter#visit_tt_structure () _struct
    | _ ->
      prerr_endline
        (Format.sprintf
          "No typed AST for %a" Fileinfos.pp_filename f) in
  Htbl.iter
    (Format.fprintf Format.std_formatter "%s %i@.")
    ident_map

let cmd =
  let open Cmdliner.Term in
  (with_common_opts (const run $ (Codebase.of_cmdline $ const !Configuration.tool_name))),
  (info "Value Use Counter")

let () =
  Cmdliner.Term.(exit @@ eval cmd)
