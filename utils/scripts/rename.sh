#!/bin/bash

# The directory containing necessary testbed-specific scripts is expected as the
# first parameter of the script. We check that it is accessible.

if [ $# -lt 1 ]; then
  echo "Please specify the directory containing the testbed scripts!"
  exit 1
fi

CONFIG_DIR=$1

echo "config dir: $CONFIG_DIR"

# Check that the necessary scripts and files exist
if [ ! -f "$CONFIG_DIR/test_cases" ]; then
  echo "Cannot find test cases config file!"
  exit 1
fi
if [ ! -x "$CONFIG_DIR/id_test.sh" ]; then
  echo "Cannot find test case discriminator script!"
  exit 1
fi
if [ ! -x "$CONFIG_DIR/load_params.sh" ]; then
  echo "Cannot find rotor parameter load script!"
  exit 1
fi
if [ ! -x "$CONFIG_DIR/clean.sh" ]; then
  echo "Cannot find build clean script!"
  exit 1
fi
if [ ! -x "$CONFIG_DIR/build.sh" ]; then
  echo "Cannot find build script!"
  exit 1
fi
if [ ! -x "$CONFIG_DIR/copy_build_log.sh" ]; then
  echo "Cannot find build log copy script!"
  exit 1
fi

# The directory in which to store the results is expected as the second
# parameter of the script. We check that it is accessible.

if [ $# -lt 2 ]; then
  echo "Please specify a directory for storing the results!"
  exit 1
fi

RESULTS_DIR=$2

echo "results dir: $RESULTS_DIR"

if [ ! -d "$RESULTS_DIR" ]; then
  echo "Results directory does not exist!"
  exit 1
fi

# The target ID of the renaming is expected as the third parameter
if [ $# -lt 3 ]; then
  echo "Please specify the target ID of the renaming!"
  exit 1
fi

TARGET_ID=$3

echo "target ID: $TARGET_ID"

# The fourth, optional parameter to the script is the number of cores available
# for running the test case refactorings in parallel. This number is passed to
# xargs using the -P option.

CORES=$4

if [ -z "$CORES" ]; then
  CORES=1
fi

# We need to know where the testbed is.

echo "testbed path: $TESTBED_PATH"

if [ ! -d "$TESTBED_PATH" ]; then
  echo "Cannot find test bed!"
  exit 1
fi

# Keep a record of the base directory.

BASE_DIR="`pwd`/"

# Check the the refactoring executable exists

EXE="$BASE_DIR/rotor"

if [ ! -x "$EXE" ]; then
  echo "Cannot find main executable!"
  exit 1
fi

# Export these variables so they are available in the run_test function that is
# called in a subshell by xargs, below
export EXE
export BASE_DIR
export CONFIG_DIR
export RESULTS_DIR
export TARGET_ID

cd "$CONFIG_DIR"

# This sets the INCLUDE_PARAMS and INPUT_DIRS variables which are used to pass
# parameters to the refactoring executable in the run_test function below.
source load_params.sh

# This function runs one refactoring test case
function run_test {

  ID=$1

  CHAIN=$("$BASE_DIR/utils/toplevel.sh" "$BASE_DIR/utils/scripts/factorise_identifier.ml" ${ID})

  if [ -z "$CHAIN" ]; then
    return 0
  elif ! "$CONFIG_DIR/id_test.sh" "$CHAIN"; then
    return 0
  elif [ -f "$RESULTS_DIR/$ID.log" ]; then
    return 0
  fi

  FILEDEPS=()
  if [ -f "$CONFIG_DIR/moddeps" ]; then
    FILEDEPS+=( --deps-from "$CONFIG_DIR/moddeps" )
  fi

  OUTPUT=$(cd "$TESTBED_PATH" && \
    "$EXE" rename $INCLUDE_PARAMS $INPUT_DIRS "${FILEDEPS[@]}" \
      --log-file "$RESULTS_DIR/$ID.log" \
      --rdeps "$RESULTS_DIR/$ID.deps" \
      $CHAIN $TARGET_ID 2>&1)

  if [ $? -ne 0 ]; then

    echo "$OUTPUT" > "$RESULTS_DIR/$ID.error"
    echo "$ID REFACTORING_FAILED"

  else

    echo "$OUTPUT" > "$RESULTS_DIR/$ID.patch"

  fi

}

# Export this function so that it is available in the subshell called by xargs, below
export -f run_test

# Generate a list of the identifiers that do not correspond to operators, to be
# piped to xargs below

cd "$BASE_DIR"

IDS=""
XARGS_IDS=""
while read ID COUNT || [[ -n "$ID" ]]; do
  if [[ "$ID" =~ ^[\._a-zA-Z\']*$ ]]; then
    IDS="$IDS $ID"
    # Add quotes around each ID and separate with \n for passing to xargs
    XARGS_IDS="$XARGS_IDS\n\"$ID\""
  fi
done < "$CONFIG_DIR/test_cases"

# Run the refactorings through xargs to take advantage of parallelism
echo -e $XARGS_IDS | xargs -P$CORES -I {} bash -c 'run_test "{}"'

# Now go through the results, and check the build status of all the refactoring
# test cases that didn't fail

# We don't run these test cases in parallel with xargs because jbuilder/dune
# already detects and uses available parallelism.

if [ -z "$REFACTOR_ONLY" ]; then

  # First get the list of object files in the compiled testbed

  cd "$TESTBED_PATH"

  OBJFILES=$("$BASE_DIR/utils/toplevel.sh" "$BASE_DIR/utils/scripts/enumerate_objfiles.ml" $INCLUDE_PARAMS $INPUT_DIRS)
  export OBJFILES

  # Now test whether each case rebuilds and produces matching object code

  cd "$RESULTS_DIR"

  for ID in $IDS; do
    PATCH_FILE="$ID.patch"
    if [ -f "$PATCH_FILE" ]; then
      "$BASE_DIR/utils/scripts/test_build.sh" "$PATCH_FILE"
    fi
  done

fi

# Now generate statistics
cd "$RESULTS_DIR"
"$BASE_DIR/utils/scripts/make_test_stats.sh"

# Return to the base directory
cd "$BASE_DIR"