FROM ocaml/opam2

LABEL maintainer="Reuben N. S. Rowe (reuben.rowe@rhul.ac.uk)"

ARG DEBIAN_FRONTEND=noninteractive

ENV TERM linux

USER root

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && apt-get install -y --no-install-recommends \
  libpcre3-dev \
  m4 \
  patchutils \
  pkg-config \
  rlwrap \
  software-properties-common \
&& sudo rm -rf /var/lib/apt/lists/*

RUN (curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash) && \
  apt-get update && \
  apt-get install -y --no-install-recommends git-lfs && \
  rm -rf /var/lib/apt/lists/*

USER opam

# 4.05 is the latest compiler version that we can build the Jane Street testbed on
RUN opam switch 4.05

WORKDIR /home/opam
RUN git clone https://gitlab.com/trustworthy-refactoring/refactorer && \
  cd refactorer && \
  git checkout dune-port && \
  eval `opam config env` && \
  opam install -y cmdliner cppo containers dune ocamlfind ocamlgraph logs pcre mparser ppxlib visitors && \
  make install && \
  opam pin add -k path rotor .

RUN opam switch create rotor-testbed 4.05.0 && \
  opam install -y jbuilder.1.0+beta11 ocaml-migrate-parsetree octavius re

WORKDIR /home/opam
RUN eval `opam config env`; \
  mkdir testbeds && \
  mkdir testbeds/jane-street && \
  tar -xzf ./refactorer/test/jane-street/js-testbed.tar.gz -C testbeds/jane-street && \
  cd testbeds/jane-street && \
  jbuilder build

RUN opam switch 4.05

ENV JANE_STREET_PATH /home/opam/testbeds/jane-street

WORKDIR /home/opam/refactorer