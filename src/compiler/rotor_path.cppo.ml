open Containers

open Compiler_import

open Rotor_longident

include Path

let dot_map ?(hd=(fun x -> x)) ?(tl=(fun x -> x)) =
  function
  #if OCAML_MINOR < 8
  | Pdot (p, id, stamp) ->
    Pdot ((hd p), (tl id), stamp)
#else
  | Pdot (p, id) ->
    Pdot ((hd p), (tl id))
#endif
  | p ->
    invalid_arg (Format.sprintf "%s.dot_map: %a" __MODULE__ Printtyp.path p)

let dest_ident p =
  match p with
  | Pident id -> id
  | _ ->
    invalid_arg (Format.sprintf "%s.dest_ident: %a" __MODULE__ Printtyp.path p)

let dest_dot =
  function
#if OCAML_MINOR < 8
  | Pdot (p, id, _) ->
#else
  | Pdot (p, id) ->
#endif
    p, id
  | p ->
    invalid_arg (Format.sprintf "%s.dest_dot: %a" __MODULE__ Printtyp.path p)

let dest_apply =
  function
  | Papply(p, p') ->
    p, p'
  | p ->
    invalid_arg (Format.sprintf "%s.dest_apply: %a" __MODULE__ Printtyp.path p)
    
let rec to_longident p =
  match p with
  | Pident id -> Lident (Ident.name id)
  | Pdot _ ->
    let p, id = dest_dot p in
    Ldot (to_longident p, id)
  | Papply (p, p') -> Lapply (to_longident p, to_longident p')

let rec all_prefixes p acc = 
  match p with
  | Pident id ->
    p :: acc
  | Pdot _ ->
    let p', _ = dest_dot p in
    let acc = p :: acc in
    all_prefixes p' acc
  | Papply(p1, p2) ->
    let acc = p :: acc in
    let ps = all_prefixes p1 acc in
    all_prefixes p2 ps
let all_prefixes ?(self=true) p =
  all_prefixes p []

let rec drop acc p p' =
  if same p p' then
    match acc with
    | [] ->
      None
    | _ ->
      Some (build acc)
  else
    match p' with
    | Pident _ ->
      invalid_arg (Format.sprintf "%s.drop" __MODULE__)
    | Pdot _ ->
      let p', id = dest_dot p in
      drop (id::acc) p p'
    | Papply _ ->
      invalid_arg
        (Format.sprintf "%s.drop: Papply not yet supported!" __MODULE__)
let drop p p' = drop [] p p'