(** This module provides functionality for setting the compiler's include path
    from the command line, and a combinator that adds this functionality to a
    given command line term, along with setup of logging functionality and the
    sending of the standard output to a file. *)

val include_dirs : string list Cmdliner.Term.t

val init_path : string list -> unit

val with_common_opts : 'a Cmdliner.Term.t -> 'a Cmdliner.Term.t