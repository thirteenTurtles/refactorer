(** Abstract view of output functionality. The default behaviour is to send to
    standard output, but this can be overridden to send to a file instead. *)

val output_file : string option Cmdliner.Term.t

val set : string -> unit
(** Set the name of the file to send output to. *)

val print_endline : string -> unit
(** Send the string to the output followed by a newline. *)

val print_string : string -> unit
(** Send the string to the output. *)
