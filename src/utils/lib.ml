open Containers
open   Format
open   Fun

open Compiler

open Asttypes

module Loc = Location
module TT = Typedtree

(******** STRING CONVERSIONS *********)

let mk_to_string print loc =
  let buf = Buffer.create 0 in
  let fmt = Format.formatter_of_buffer buf in
  let _ = print fmt loc in
  let _ = Format.pp_print_flush fmt () in
  Buffer.contents buf

(******** ERROR HANDLING *********)

let output_warning ?loc str =
  prerr_endline (Format.sprintf "Warning: %s" str) ;
  Option.map_or ~default:()
  (fun loc ->
    prerr_char '\t' ;
    Location.print_loc Format.err_formatter loc)
  loc

let not_implemented () = failwith "Not implemented!"

(************ FORMAT ***************)
let after_char c f fmt = Format.fprintf fmt "%c%a" c f

(************ AST ***************)

module Tokens = struct
  let space = ' '
  let labelled_arg_signifier = '~'
  let arg_label_terminator = ':'
  let comment_begin = "(*"
  let comment_end = "*)"

  let brackets = "[", "]"

  let mk_arg_label ?(signifier=true) lbl =
    Format.sprintf "%s%s%c"
      (if signifier then (String.of_char labelled_arg_signifier) else "")
      lbl arg_label_terminator
  let mk_field_label lbl = Format.sprintf "%s = " lbl
end

open Tokens

module ApplyTokens = struct
  let brackets f = uncurry f brackets
end

(* Strips comments out of a string.
   Assumes that the comments are well formed.
   Handles nested comments. *)
let strip_comments str =
  let len = String.length str in
  let result = Buffer.create len in
  let stack_counter, pos, last_pos = ref 0, ref 0, ref 0 in
  while (!pos < len - 1) do
    incr pos ;
    match str.[!pos - 1] with
    | '(' ->
        if Char.equal str.[!pos] '*' then begin
          if !stack_counter = 0 then
            Buffer.add_string result
              (String.sub str !last_pos (!pos - !last_pos - 1)) ;
          incr stack_counter ;
          incr pos ;
        end
    | '*' ->
        if Char.equal str.[!pos] ')' then begin
          decr stack_counter ;
          incr pos ;
          if !stack_counter = 0 then last_pos := !pos;
        end
    | _ -> ()
  done ;
  Buffer.add_string result
    (String.sub str !last_pos (len - !last_pos)) ;
  Buffer.contents result

let lines_between_buf (p, p') lines buf =
  let open Lexing in
  let line, offset = p.pos_lnum - 1, p.pos_cnum - p.pos_bol in
  let line', offset' = p'.pos_lnum - 1, p'.pos_cnum - p'.pos_bol in
  let line_end =
    let n =
      if line = line'
        then offset' - offset
        else (String.length lines.(line)) - offset in
    String.sub lines.(line) offset n in
  let lines' =
    if line = Array.length lines || line = line'
      then []
      else Array.(to_list (sub lines (line + 1) (line' - line - 1))) in
  let line_start =
    let start = if line = line' then offset' else 0 in
    String.sub lines.(line') start (offset' - start) in
  Buffer.add_string buf line_end ;
  if not (Int.equal line line') then Buffer.add_char buf '\n' ;
  Buffer.add_string buf (String.unlines lines') ;
  Buffer.add_string buf line_start ;
  buf

let lines_between ((p, p') as range) lines =
  let buf =
    Buffer.create (max 0 ((p'.Lexing.pos_lnum - p.Lexing.pos_lnum) * 80)) in
  Buffer.contents (lines_between_buf range lines buf)

(*** FILENAME MANIPULATION ***)
let get_mli f =
  (Filename.remove_extension f) ^ !Config.interface_suffix

(*** FILE SYSTEM ***)
let prng_state = Random.State.make_self_init ()

(* Adapted from Jane Street Core_filename module (tmp_dir and retry). *)
let tmp_dir prefix =
  let rec try_name counter =
    let name =
      sprintf "%s_%06x"
        prefix (Random.State.bits prng_state) in
    let name = Filename.concat (Filename.get_temp_dir_name ()) name in
    try
      Unix.mkdir name 0o700 ; name
    with Sys_error _ | Unix.Unix_error _ as e ->
      if counter >= 1000 then raise e else try_name (counter + 1)
  in
  try_name 0

(*** PARSING ***)
let handle_reply =
  function
  | MParser.Success res -> res
  | MParser.Failed(msg, _) -> failwith msg

let mk_of_string parse s =
  handle_reply (MParser.parse_string parse s ())
let mk_of_channel parse c =
  handle_reply (MParser.parse_channel parse c ())

(*** LIST FUNCTIONS ***)
let pivot_map ?(rev=true) f =
  let rec pivot_map acc = function
  | [] ->
    None
  | x::xs ->
    match f x with
    | None ->
      pivot_map (x::acc) xs
    | Some v ->
      let acc = if rev then acc else List.rev acc in
      Some (acc, v, xs) in
  pivot_map []

let pivot_pred ?(rev=true) p =
  let rec pivot_pred acc = function
  | [] ->
    None
  | x::xs ->
    if p x then
      let acc = if rev then acc else List.rev acc in
      Some (acc, x, xs)
    else
      pivot_pred (x::acc) xs in
  pivot_pred []

(*** Utility Modules and Functors ***)
module OrderedPair
  (X : sig type t val compare : t -> t -> int end)
  (Y : sig type t val compare : t -> t -> int end) =
struct
  type t = X.t * Y.t
  let compare (x, y) (x', y') =
    let x_cmp = X.compare x x' in
    if x_cmp = 0 then
      Y.compare y y'
    else
      x_cmp
end

module SetWithMonoid = struct
  module type S = sig
    include Set.S
    class monoid : object
      method private zero : t
      method private plus : t -> t -> t
    end
  end
  module Make(X : Set.OrderedType) = struct
    include Set.Make(X)
    class monoid = object
      method private zero = empty
      method private plus = union
    end
  end
end
