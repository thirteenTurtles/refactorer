ROOT := $(shell pwd)

BUILD_DIR := _build/default

OCAML_PATH := $(strip $(shell opam config var lib || echo -n ""))
OCAML_PATH := $(if $(OCAML_PATH),$(OCAML_PATH),$(shell (ocamlfind printconf path || echo -n "") | (read dir; echo -n "$$dir")))

MAIN_MODULE := main
MAIN_TARGET := src/$(MAIN_MODULE).exe
MAIN_EXE := $(BUILD_DIR)/$(MAIN_TARGET)

export ROOT BUILD_DIR MAIN_EXE OCAML_PATH

PT_PRINTER_UTIL := utils/parsetree_printer.exe
TT_PRINTER_UTIL := utils/typedtree_printer.exe

BUILD := dune build
CLEAN := dune clean

.PHONY: all install utils clean

all:
	$(BUILD) @all
	$(BUILD) $(MAIN_TARGET)

install:
	$(BUILD) @install

utils:
	$(BUILD) $(PT_PRINTER_UTIL)
	$(BUILD) $(TT_PRINTER_UTIL)

clean:
	$(CLEAN)

tests.%:
	$(MAKE) -C test --no-print-directory $*
